Gyant Code Challenge - Case Labeling
===

### Requirements
Node version: v10.18.0

### Instructions

- Clone the repository
```bash
$ git clone git@bitbucket.org:el-der/gyant-case-labeling.git
```

- Install dependencies
```bash
$ cd gyant-case-labeling
$ npm install
```

- Copy parameters

```bash
$ cp config/parameters.dist.json config/parameters.json
```

- Configuration

    Edit the file 'config/parameters.json' and change the field 'gyantCaseLabelingUri' with a mongo connection URI, e.g. 'mongodb://localhost:27017/gyant-case-labeling?replicaSet=rs'
    
- Database initialization
```bash
$ npm run data-bootstrap
```

- Start the service
```bash
$ npm run start
```