const caseLabeling = require("../data/CaseLabeling")
const csv = require('csv-parser')
const fs = require('fs')
const path = require("path")

const bootstrapPath = path.join(__dirname + "../../../config/bootstrap")

async function loadConditionsData() {
    return new Promise(function (resolve, reject) {
        const results = []
        fs.createReadStream(path.join(bootstrapPath, 'conditions.csv'))
            .pipe(csv({
                separator: '\t',
                mapHeaders: ({header, index}) => index === 0 ? 'code' : 'description'
            }))
            .on('data', (data) => results.push(data))
            .on('end', () => resolve(results))
            .on('error', reject)
    })
}

async function loadConditions(conditionModel) {
    const now = new Date()
    let conditionsData = await loadConditionsData()
        .then(data => data
            .map(function (item) {
                return {
                    insertOne: {
                        document: {
                            ...item,
                            createdAt: new Date(),
                            updatedAt: now
                        }
                    }
                }
            }))

    await conditionModel.deleteMany()
    await conditionModel.collection.bulkWrite(conditionsData)
}

async function loadCases(caseModel) {
    const casesPath = path.join(bootstrapPath, 'cases')
    const files = fs.readdirSync(casesPath)
    const cases = []
    const now = new Date()
    for (const file of files) {
        const ehr = fs.readFileSync(path.join(casesPath, file)).toString()
        cases.push({
            ehr,
            conditionRef: null,
            labeledAt: null,
            createdAt: new Date(),
            updatedAt: now
        })
    }

    let casesData = cases.map(function (item) {
        return {
            insertOne: {
                document: item
            }
        }
    })

    await caseModel.deleteMany()
    await caseModel.collection.bulkWrite(casesData)
}

async function dataBootstrap() {
    const labeling = (await caseLabeling())

    await loadConditions(labeling.models.conditionModel)

    // TODO: do proper output
    console.info("-> Conditions Data imported")

    await loadCases(labeling.models.caseModel)

    // TODO: do proper output
    console.info("-> Cases Data imported")
    console.info("-> Done")

    labeling.connection.close()
}

dataBootstrap().catch(err => {
    // TODO: do proper error output
    console.error(err)
    process.exit(1)
})
