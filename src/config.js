process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const parameters = require('../config/parameters')

const config = {
    port: process.env.PORT || parameters.port || 7000,
    gyantCaseLabelingUri: parameters.gyantCaseLabelingUri,
    debug: parameters.debug === true
}

if (!config.port) {
  throw new Error('PORT configuration is required')
}

if (!config.gyantCaseLabelingUri) {
    throw new Error('Gyant Case Labeling Connection string is missing')
}

module.exports = config
