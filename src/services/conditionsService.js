const {conditionsRepository} = require('../repositories')
const {EntityNotFoundException} = require('./serviceExceptions')

class ConditionsService {

    /**
     * Return all conditions
     * @returns {Promise<[ConditionType]>}
     */
    async findAll() {
        // TODO: Cache this result
        return conditionsRepository.findAll()
    }

    /**
     * @throws
     * @param filter {ConditionFilter}
     * @returns {Promise<ConditionType|null>}
     */
    async single(filter) {
        const condition = conditionsRepository.findOne(filter)
        if (!condition) {
            throw new EntityNotFoundException('Condition not found')
        }

        return condition
    }

    /**
     * Checks if exists conditions according to the filter
     * @param filter {ConditionFilter|undefined}
     * @returns {Promise<number>}
     */
    async exists(filter) {
        return conditionsRepository.count(filter)
    }
}

module.exports = new ConditionsService()
