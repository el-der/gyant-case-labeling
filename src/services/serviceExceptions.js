
class ServiceException extends Error {
    /**
     * @param message {string}
     */
    constructor(message) {
        super(message)
    }
}

class ServiceValidationException extends ServiceException {
    /**
     * @param message {string}
     */
    constructor(message) {
        super(message)
    }
}

class EntityNotFoundException extends ServiceException {
    /**
     * @param message {string}
     */
    constructor(message) {
        super(message)
    }
}

module.exports.ServiceException = ServiceException
module.exports.ServiceValidationException = ServiceValidationException