const {casesRepository} = require('../repositories')
const conditionsService = require('./conditionsService')
const {ServiceValidationException} = require('./serviceExceptions')

class CasesServices {

    /**
     * Return all cases
     * @returns {Promise<[CaseType]>}
     */
    async findAll() {
        return casesRepository.findAll()
    }

    /**
     * Find the next Case not yet labeled
     * @returns {Promise<CaseType|null>}
     */
    async getNextCase() {
        return casesRepository.findOne({
            conditionId: null
        }, {
            createdAt: "asc"
        })
    }

    /**
     * Checks if exists cases according to the filter
     * @param filter {CaseFilter|undefined}
     * @returns {Promise<number>}
     */
    async exists(filter) {
        return casesRepository.count(filter)
    }

    async assignCaseCondition(caseId, conditionCode) {
        const condition = await conditionsService.single({
            code: conditionCode
        })

        if (!await this.exists({id: caseId})) {
            throw new ServiceValidationException(`Cases '${caseId}' not found`)
        }

        await casesRepository.update(caseId, {
            conditionId: condition.id,
            labeledAt: new Date()
        })
    }
}

module.exports = new CasesServices()
