const config = require("../config")
const hbs = require( 'express-handlebars')
const bodyParser = require( 'body-parser')

/**
 * Express App initialization
 * @param app {Application}
 */
module.exports = async function(app) {

    app.set("port", config.port)

    app.set('view engine', 'hbs');

    app.use(bodyParser.urlencoded({ extended: true }))

    app.engine( 'hbs', hbs( {
        extname: 'hbs',
        defaultLayout: 'main'
    }))

    app.use(await require('../controllers')())
}
