const appInitializer = require('./appInitializer')
const caseLabelingInitializer = require("../data/CaseLabeling")

/**
 * Calls the initialization of the different components of the application
 *
 * @param expressApp {Application}
 * @returns {Promise<void>}
 */
module.exports = async function(expressApp) {
    await caseLabelingInitializer()
    await appInitializer(expressApp)
}
