let express = require('express')

async function initServer() {

    const app = express()

    await require('./initializers')(app)

    return app.listen(app.get("port"), () => {

        // TODO: Do some proper logging
        console.log(
            'App is running at http://localhost:%d in %s mode',
            app.get("port"),
            app.get("env")
        )

        console.log("  Press CTRL-C to stop\n")
    })
}

initServer().catch(function (err) {
    console.log('Error starting server: '  + err.message)
})
