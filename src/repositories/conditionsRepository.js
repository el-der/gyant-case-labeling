const {conditionModel} = require('../data/CaseLabeling/models')


/**
 * @typedef ConditionFilter
 * @type {object}
 * @property {string|undefined} id
 * @property {string|undefined} code
 */

class ConditionsRepository {

    /**
     * Access and loads all condition from the condition repository
     * @returns {Promise<[ConditionType]>}
     */
    async findAll() {
        return conditionModel.find()
            .then(result => result.map(p => p.toObject()))
    }

    /**
     * Returns the first Condition find according to the filter and sort
     * @param filter {ConditionFilter|undefined}
     * @returns {Promise<ConditionType|null>}
     */
    async findOne(filter) {
        return conditionModel.findOne(this._buildFilterCriteria(filter))
            .then(result => result ? result.toObject() : null)
    }

    /**
     * Checks if exists conditions according to the filter
     * @param filter {ConditionFilter|undefined}
     * @returns {Promise<boolean>}
     */
    async exists(filter) {
        return (await this.count(filter)) > 0
    }

    /**
     * Return the conditions count according to the filter
     * @param filter {ConditionFilter|undefined}
     * @returns {Promise<number>}
     */
    async count(filter) {
        return conditionModel.countDocuments(this._buildFilterCriteria(filter))
    }

    /**
     * Transforms the provided filter into criteria for mongo query
     * @param filter {ConditionFilter|undefined}
     * @returns {{}}
     * @private
     */
    _buildFilterCriteria(filter) {
        const criteria = {}
        if (filter) {
            if (filter.id) {
                criteria._id = filter.id
            }
            criteria.code = filter.code
        }
        return criteria
    }
}

module.exports = new ConditionsRepository()
