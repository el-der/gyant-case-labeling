const {caseModel} = require('../data/CaseLabeling/models')

/**
 * @typedef CaseFilter
 * @type {object}
 * @property {string} id
 * @property {string|null|undefined} conditionId
 */

/**
 * @typedef CaseSort
 * @type {object}
 * @property {"asc"|"desc"|undefined} createdAt
 */

/**
 * @typedef CaseUpdate
 * @type {object}
 * @property {string} conditionId
 * @property {Date} labeledAt
 */

class CasesRepository {

    /**
     * Access and loads all cases from the cases repository
     * @returns {Promise<[CaseType]>}
     */
    async findAll() {
        return caseModel.find()
            .then(result => result.map(p => p.toObject()))
    }

    /**
     * Returns the first Case according to the filter and sort
     * @param filter {CaseFilter|undefined}
     * @param sort {CaseSort}
     * @returns {Promise<CaseType|null>}
     */
    async findOne(filter, sort) {
        return caseModel.findOne(this._buildFilterCriteria(filter))
            .sort(sort)
            .then(result => result ? result.toObject() : null)
    }

    /**
     * Updates the case data on the repository
     * @param id {string}
     * @param data {CaseUpdate}
     * @returns {Promise<boolean>}
     */
    async update(id, data) {
       let result = await caseModel.updateOne({
            _id: id
        }, {
           conditionRef: data.conditionId,
           labeledAt: data.labeledAt
       })
       return result.n === 1
    }

    /**
     * Checks if exists cases according to the filter
     * @param filter {CaseFilter|undefined}
     * @returns {Promise<boolean>}
     */
    async exists(filter) {
        return (await this.count(filter)) > 0
    }

    /**
     * Return the cases count according to the filter
     * @param filter {CaseFilter|undefined}
     * @returns {Promise<number>}
     */
    async count(filter) {
        return caseModel.countDocuments(this._buildFilterCriteria(filter))
    }

    _buildFilterCriteria(filter) {
        const criteria = {}
        if (filter) {
            if (filter.id) {
                criteria._id = filter.id
            }
            criteria.conditionRef = filter.conditionId
        }
        return criteria
    }
}

module.exports = new CasesRepository()
