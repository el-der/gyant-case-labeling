const connection = require("../caseLabelingConnection").connection
const mongoose = require("mongoose")

/**
 * @typedef CaseType
 * @type {object}
 * @property {string} id - Case ID.
 * @property {string} ehr - Condition Electronic Health Record.
 * @property {(string|null)} conditionRed - Reference to the condition assign to this case.
 * @property {(Date|null)} labeledAt - Date and Time on which the case was labeled.
 */

const Case = new mongoose.Schema(
    {
        ehr: {
            type: String,
            required: [true, 'The EHR is required']
        },
        conditionRef: {
            type: mongoose.Types.ObjectId,
            default: null,
            required: false
        },
        labeledAt: {
            type: Date,
            default: null,
            required: false
        }
    },
    {
        timestamps: true,
        toObject: {virtuals: true}
    }
)

Case.virtual('id').get(function(){
    return this._id.toHexString();
})

module.exports = connection.model('case', Case, "cases")
