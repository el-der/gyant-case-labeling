const connection = require("../caseLabelingConnection").connection
const mongoose = require("mongoose")

/**
 * @typedef ConditionType
 * @type {object}
 * @property {string} id - Condition ID.
 * @property {string} code - Condition code.
 * @property {string} description - Condition description.
 */

const Condition = new mongoose.Schema(
    {
        code: {
            type: String,
            required: [true, 'The condition requires a unique code'],
            unique: true
        },
        description: String,
    },
    {
        timestamps: true,
        toObject: {virtuals: true}
    }
)

Condition.virtual('id').get(function(){
    return this._id.toHexString();
})

module.exports = connection.model('condition', Condition, "conditions")
