const connInitializer = require("./caseLabelingConnection")
const caseLabelingModels = require("./caseLabelingModels")

/**
 * Returns a object with the Case labeling connection and models already initialized
 * @returns {Promise<{models: {conditionModel, caseModel}, connection}>}
 */
module.exports = async function () {
    const connection = await connInitializer.init()
    const models = await caseLabelingModels()
    return {
        connection,
        models
    }
}
