const config = require("../../config")
const mongoose = require("mongoose")

class CaseLabelingConnector {
    async init() {
        if (this._connection) {
            throw new Error('The Case Labeling connection has already been initialized.')
        }

        this._connection = await mongoose.createConnection(
            config.gyantCaseLabelingUri, {
                useUnifiedTopology: true,
                useNewUrlParser: true,
                useCreateIndex: true
            })

        this._connection.set("debug", config.debug)

        // TODO: handle and logging connection events and errors

        return this._connection
    }

    /**
     * Case Labeling connection
     * @type {Connection|null}
     */
    get connection() {
        return this._connection
    }
}

module.exports = new CaseLabelingConnector()
