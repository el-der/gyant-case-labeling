/**
 * This module will serve as initializer of the mongoose models.
 * The mongoose models don't user the mongoose default connection. They use a caseLabelingConnection directly.
 * This is useful if the project handle with multiple mongoose connections.
 * On other hand, this approach requires a connection and models initialization.
 */

/**
 * Returns a object with all modules
 * @returns {Promise<{conditionModel, caseModel}>}
 */
module.exports = async function () {
    return {
        conditionModel: require("./models/conditionModel"),
        caseModel: require('./models/caseModel')
    }
}
