const {conditionsService, casesServices, exceptions} = require('../services')

/**
 * Build the routes for the cases pages
 * @param router {Router}
 * @returns {Promise<void>}
 */
module.exports = async function(router) {

    /**
     *
     * @param req {Request}
     * @param res {Response}
     * @returns {Promise<void>}
     */
    async function caseLabelingRender(req, res) {
        const conditions = await conditionsService.findAll()

        const nextCase = await casesServices.getNextCase()

        res.render('caseLabeling', {
            conditions,
            case: nextCase
        })
    }

    router.get('/case-labeling', async function (req, res) {

        return caseLabelingRender(req, res)

    })

    router.post('/case-labeling', async function (req, res) {

        if (req.body.caseId && req.body.conditionCode) {
           await casesServices.assignCaseCondition(req.body.caseId, req.body.conditionCode)
               .catch(error => {
                   if (error instanceof exceptions.ServiceException) {
                       // TODO: Handle service exceptions
                       console.error(error)
                   } else {
                       // TODO: Handle unexpected exceptions
                       console.error(error)
                   }
               })
        }

        return caseLabelingRender(req, res)
    })
}
