/**
 * Build the routes for the home pages
 * @param router {Router}
 * @returns {Promise<void>}
 */
module.exports = async function(router) {
    router.get('/', function (req, res) {
        res.render('home/index')
    })
}
