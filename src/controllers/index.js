const express = require('express')
const status = require('./appStatus')
const home = require('./home')
const cases = require('./cases')

/**
 * Returns a initialized router with the application implemented paths
 * @returns {Promise<Router>}
 */
module.exports = async function() {
    const router = new express.Router()

    await status(router)
    await home(router)
    await cases(router)

    return router
}
