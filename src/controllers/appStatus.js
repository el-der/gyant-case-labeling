/**
 * Build the routes for the application status
 * @param router {Router}
 * @returns {Promise<void>}
 */
module.exports = async function(router) {
    router.get('/_status', function (req, res) {
        res.status(200)
            .end()
    })

    router.head('/_status', function (req, res) {
        res.status(200)
            .end()
    })
}
